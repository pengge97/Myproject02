package com.jt.controller;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import lombok.experimental.Accessors;

@RestController
@ConfigurationProperties(prefix = "msg")//批量属性赋值
@Data
@Accessors(chain = true)   //开启链式加载结构
public class MsgController {
	
	//@Value("${msg.username}")
	private String username;
	//@Value("${msg.age}")
	private String age;
	
	

	@RequestMapping("/getMsg")
	public String getMsg() {
//		MsgController controller = new MsgController();  
//		controller.setUsername(username).setAge(age);   //链式加载结构
		return "结果为:"+username+":"+age;
	}
}
	