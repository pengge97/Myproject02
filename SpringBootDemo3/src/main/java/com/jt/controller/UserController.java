package com.jt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
@Controller
@RequestMapping("/")
public class UserController {

	/**
	 * 查询User表的信息
	 * 1.url:localhost:8090/findAll
	 * 2.参数:无
	 * 3.返回结果
	 * 
	 */
	@Autowired
	private UserMapper userMapper;
	@RequestMapping("findAll")
	public String doFindAll(Model model) {
//		List<User> userList = userMapper.selectList(null);
//		System.out.println(userList);
//		model.addAttribute("userList", userList);
//		return "userList";
		return "ajax";
	}
	@RequestMapping("findObjects")
	@ResponseBody
	public Object name() {
		List<User> userList = userMapper.selectList(null);
		return userList;
	}
}
