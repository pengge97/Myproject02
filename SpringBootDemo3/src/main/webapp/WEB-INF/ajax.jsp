<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>您好Springboot</title>
<script type="text/javascript" src="/js/jquery-3.5.1.min.js"></script>
</head>
<body>
	<table border="1px" width="65%" align="center">
		<tr>
			<td colspan="6" align="center"><h3>学生信息</h3></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>性别</th>
			<th></th>
		</tr>
		
		<tbody id= "tbodyId"></tbody>
	</table>
</body>
<script type="text/javascript">
$(function(){
	dofindObject();
});

function dofindObject(){
	
	$.get("findObjects",function(result){
		doCreateTable(result);
		});


	//常规ajax.
	/*$.ajax({
		type:"get",
		url:"findObject",
		//data:    没有参数
		success:function(result){

			},
		error:function(result){
			alert("ajax请求异常");

			},
		async:true //默认为true,为异步请求,false为同步请求,
		cache:true //缓存页面信息,默认为true,fasle表示不缓存
		});*/
}
function doCreateTable(data){
	
	for(var i=0 ; i < data.length;i++){
		$("#tbodyId").append(doCreateTr(data[i]));
	}
}
function doCreateTr(u){
	return '<tr><th>'+u.id+'</th><th>'+u.name+'</th><th>'+u.age+'</th><th>'+u.sex+'</th></tr>';
}
</script>
</html>