package com.jt.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.common.bytecode.Wrapper;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.DubboCartMapper;
import com.jt.pojo.Cart;
import com.jt.service.DubboCartService;
@Service
public class DubboCartServiceImpl implements DubboCartService{
	@Autowired
	private DubboCartMapper cartMapper;
	@Override
	public List<Cart> findCartListByUserId(Long userId) {
		QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", userId);
		List<Cart> cartList = cartMapper.selectList(queryWrapper);
		return cartList;
	}
	@Override
	public void updateCartNum(Cart cart) {
		Cart cartTemp = new Cart();
		cartTemp.setNum(cart.getNum())
				.setUpdated(new Date());
		UpdateWrapper<Cart> updateWrapper = new  UpdateWrapper<>();
		updateWrapper.eq("item_id", cart.getItemId())
					.eq("user_id", cart.getUserId());
					
		
		cartMapper.update(cartTemp, updateWrapper);
		
	}
	//记一次加入购物车时,将商品加入购物车,大于一次时,只做更改
	@Override
	public void saveCart(Cart cart) {
		//1.查询数据库中是否有该记录itemId和userId
		QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("item_id", cart.getItemId())
					.eq("user_id", cart.getUserId());
		Cart cartDB = cartMapper.selectOne(queryWrapper);
		if (cartDB == null) {
			//没有这个数据
			cart.setCreated(new Date())
				.setUpdated(cart.getCreated());
			cartMapper.insert(cart);
		}else {
			//值更新商品数量
			long id = cartDB.getId();
			int num = cart.getNum()+cartDB.getNum();
			Date date = new Date();
			cartMapper.updateCartNum(id,num,date);
			
		}
	}
	//删除对应的商品
	@Override
	public void deleteByItemId(Cart cart) {
		QueryWrapper<Cart> wrapper = new QueryWrapper<>();
		wrapper.eq("user_id", cart.getUserId())
				.eq("item_id", cart.getItemId());
		cartMapper.delete(wrapper);
		
	}

}
