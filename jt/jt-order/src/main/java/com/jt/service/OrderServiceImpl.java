package com.jt.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.anno.InsertOperation;
import com.jt.mapper.OrderItemMapper;
import com.jt.mapper.OrderMapper;
import com.jt.mapper.OrderShippingMapper;
import com.jt.pojo.Order;
import com.jt.pojo.OrderItem;
import com.jt.pojo.OrderShipping;

@Service
public class OrderServiceImpl implements DubboOrderService {
	
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private OrderShippingMapper orderShippingMapper;
	@Autowired
	private OrderItemMapper orderItemMapper;
	
	
	@InsertOperation(key = "rank")
	@Transactional
	@Override
	public Order saveOrder(Order order) {
		
		String orderId = "" +order.getUserId() +System.currentTimeMillis();
		Date date= new Date();
		order.setOrderId(orderId)
			.setStatus(1)
			.setCreated(date)
			.setUpdated(date);
		orderMapper.insert(order);
		System.out.println("订单入库成功");
		//订单物流入库
		OrderShipping orderShipping = order.getOrderShipping();
		orderShipping.setOrderId(orderId)
						.setCreated(date)
						.setUpdated(date);
		orderShippingMapper.insert(orderShipping);
		
		//订单商品入库
		List<OrderItem> orderItems = order.getOrderItems();
		for (OrderItem orderItem : orderItems) {
			orderItem.setOrderId(orderId)
					.setCreated(date)
					.setUpdated(date);
			orderItemMapper.insert(orderItem);
		}
		return order;
	}


	@Transactional
	@Override
	public Order findOrderById(String orderId) {
		Order entity = new Order();
		entity.setOrderId(orderId)
			  .setStatus(5);
		orderMapper.updateById(entity);
		Order order = orderMapper.selectById(orderId);
		OrderShipping orderShopping = orderShippingMapper.selectById(orderId);
		
		
		QueryWrapper<OrderItem> queryWrapper =new QueryWrapper<>();
		queryWrapper.eq("order_id", orderId);
		
		
		List<OrderItem> orderItems = orderItemMapper.selectList(queryWrapper);
		
		
		return order.setOrderShipping(orderShopping).setOrderItems(orderItems);
	}
	
	
}
