package com.jt.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.User;

@RestController
public class JSONPController {
	
	@RequestMapping("/web/testJSONP")
	public JSONPObject jsonp(String callback) {
		
		//准备返回数据
		User user = new User();
		user.setId(10L)
			.setPassword("123456")
			.setPhone("652635215");
		return new JSONPObject(callback, user);
	}
}
