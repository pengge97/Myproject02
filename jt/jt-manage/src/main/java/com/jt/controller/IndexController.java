package com.jt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
	/**
	 * url:	/page/item-add     
	 * 		/page/item-list         
	 * 		/page/item-param-list
	 * @param moduleName
	 * @return
	 * 
	 * 	restFul风格1:
	 * 
	 * @PathVariable 接收请求url中的参数
	 * 		value/name 表示接收参数的名称
	 * 		required  	该参数是否必须    默认为true
	 * 
	 * 作用1:动态获取URL路径中的参数
	 * 作用2:以统一的url地址,不同的方法类型,实现不同业务的通用
	 * 
	 * 	restFul风格2:
	 * 		例子1:http://www.jt.com/user?id=1  查询
	 * 		例子2:http://www.jt.com/user?id=2  删除
	 * 		例子3:http://www.jt.com/user?id=3  更新
	 * 	@RequestMapping(method = RequestMethod.GET) 根据method的不同匹配不同的解析器
	 */

	@RequestMapping("/page/{abc}")
	public String module(@PathVariable(value = "abc") String moduleName) {
		
		return moduleName;
	}
}
