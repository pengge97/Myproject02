package com.jt.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.ItemService;
import com.jt.vo.SysResult;

@RestController
@RequestMapping("/item/")
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	/**
	 * 业务:展现商品的列表信息,也EasyUI表格数据展现
	 * url:http://localhost:8091/item/query?page=18&rows=20
	 * 返回值:EasyUITable VO对象
	 */
	@RequestMapping("query")
	public EasyUITable findIemByPage(Integer page,Integer rows) {
		EasyUITable obj = itemService.findItemByPage(page, rows);
		return obj;
	}
	@RequestMapping("save")
	public SysResult saveItem(Item item ,ItemDesc itemDesc) {
		try {
			
			itemService.saveItem(item,itemDesc);
			return SysResult.success("保存商品信息成功",null);
		} catch (Exception e) {
			e.printStackTrace();
			return SysResult.fail();
		}
		//全局异常处理机制
	}
	
	@RequestMapping("update")
	public SysResult updateItemById(Item item,ItemDesc itemDesc) {
		item.setUpdated(new Date());
		itemService.updateItemById(item,itemDesc);
		
		return SysResult.success("商品更新成功", null);
	}
	@RequestMapping("delete")
	public SysResult deleteItemByIds(Long[] ids) {
		itemService.deleteItemByIds(ids);
		return SysResult.success("商品删除成功", null);
	}
//	@RequestMapping("reshelf")
//	public SysResult reshelfItems(Long[] ids) {
//		itemService.reshelfItems(ids);
//		return SysResult.success("上架成功", null);
//	}
//	@RequestMapping("instock")
//	public SysResult instockItems(Long[] ids) {
//		itemService.instockItems(ids);
//		return SysResult.success("下架成功", null);
//	}
	@RequestMapping("statusChange")
	public SysResult itemsRestFul(Long[] ids,Integer status) {
		itemService.itemsRestFul(ids,status);
		return SysResult.success("下架成功", null);
	}
	
	@RequestMapping("/query/item/desc/{ItemId}")
	public SysResult findItemDesc(@PathVariable Integer ItemId) {
		ItemDesc itemDesc = itemService.findItemDesc(ItemId);
		return SysResult.success(itemDesc);
	}
	
}
