package com.jt.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jt.pojo.ImageVO;
import com.jt.service.FileService;

@RestController
public class FileController {
	
	@Autowired
	FileService fileService;
	/**
	 * MultipartFile接口,负责实现文件的接收
	 * 		1.必须指定文件上传的路径信息  D:xxx\xxx\xxx.jpg
	 * 		2.将字节信息利用outPutStream进行输出操作
	 * @param fileImage
	 * @return
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@RequestMapping("/file")
	public String file(MultipartFile fileImage) throws IllegalStateException, IOException {
		
		//1.定义文件目录信息
		String dirPath = "E:/img"; //文件目录
		File fileDir=  new File(dirPath);
		//2.文件目录是否存在
		if (!fileDir.exists()) {
			fileDir.mkdirs();//如果目录不存在,创建目录
		}
		
		//3.获取文件信息     xxx.jpg
		String fileName= fileImage.getOriginalFilename();
		fileName = UUID.randomUUID()+".jpg";
		//实现文件上传.指定文件真实路径
		File file= new File(dirPath +"/" +fileName);
		fileImage.transferTo(file);
		return "文件上传成功";
	}
	
	
	
	/**
	 * 文件上传
	 * url:pic/upload
	 * params: uploadFile
	 * return:ImageVO对象
	 */
	@RequestMapping("/pic/upload")
	public ImageVO uploadFile(MultipartFile uploadFile) {
		
		return fileService.uploadFile(uploadFile);
	}
}
