package com.jt.service;

import java.util.List;

import com.jt.pojo.ItemCat;
import com.jt.vo.EasyUITree;

public interface ItemCatService {

	ItemCat findItemCatNameById(Long id);
	List<EasyUITree> findItemCatList(Long id);
	String findById(Long id);
	List<EasyUITree> findItemCatByCache(Long id);
}
