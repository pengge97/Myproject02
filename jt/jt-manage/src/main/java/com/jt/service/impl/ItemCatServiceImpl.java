package com.jt.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.anno.CacheFInd;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.util.ObjectMapperUtil;
import com.jt.vo.EasyUITree;

import redis.clients.jedis.Jedis;
@Service
public class ItemCatServiceImpl implements ItemCatService {
	@Autowired
	ItemCatMapper itemCatMapper;
	//初始化时,该注解不是必须注入,但是如果程序调用则必须有值
	@Autowired(required = false)
	private Jedis jedis;
	@Override
	public ItemCat findItemCatNameById(Long id) {
		ItemCat itemCat = itemCatMapper.selectById(id);
		return itemCat;
	}
	@Override
	@CacheFInd(key="ITEM_CAT_LIST")
	public List<EasyUITree> findItemCatList(Long parentId) {
		//查询parent_id=0的记录
		QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("parent_id", parentId);
		//一级目录
		List<ItemCat> list = itemCatMapper.selectList(queryWrapper);
		List<EasyUITree> treeList= new ArrayList<>();
		for (ItemCat itemCat : list) {
			Long id = itemCat.getId();
			String text = itemCat.getName();
			String state = itemCat.getIsParent()?"closed":"open";
			EasyUITree uiTree = new EasyUITree(id, text, state);
			treeList.add(uiTree);
		}
		return treeList;
	}

	@Override
	public String findById(Long id) {
		ItemCat entity = itemCatMapper.selectById(id);
		
		return entity.getName();
	}
	/**
	 * 1.定义key
	 * 2.根据key查询redis
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<EasyUITree> findItemCatByCache(Long id) {
		//定义key
		String key = "ITEM_CATDESC"+id;
		List<EasyUITree> treeList = new ArrayList<>();
		long start = System.currentTimeMillis();
		//判断redis中是否有值
		if (jedis.exists(key)) {
			//不是第一次查询
			String json = jedis.get(key);
			long end= System.currentTimeMillis();
			treeList = ObjectMapperUtil.toObject(json, treeList.getClass());
			System.out.println("缓存查询时间:"+(end-start));
		}else {
			//redis中没有这个key 是第一次查询
			treeList =findItemCatList(id);
			long end= System.currentTimeMillis();
			//将list转换为json
			String json = ObjectMapperUtil.toJson(treeList);
			//将数据保存到redis中
			jedis.set(key, json);
			System.out.println("数据库查询时间:"+(end-start));
		}
		return treeList;
	}
}
