package com.jt.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;

	/**
	 * sql:select xxx from tb_item limit startIndex,pagesize;
	 */
//	@Override
//	public EasyUITable findItemByPage(Integer page, Integer rows) {
//		//获取总记录数
//		Integer total = itemMapper.selectCount(null);
//		
//		//查询分页后的结果
//		int startIndex = (page-1)*rows;
//		List<Item> row = itemMapper.findItemByPage(startIndex, rows);
//		return new EasyUITable(total, row);
//	}
	
	@Override
	public EasyUITable findItemByPage(Integer page, Integer rows) {
		//定义分页对象
		Page<Item> mppage = new Page<>(page, rows);
		//定义条件构造器
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("updated");
		//获取查询后的Page对象
		Page<Item> pageObj = itemMapper.selectPage(mppage, queryWrapper);
		return new EasyUITable((int)pageObj.getTotal(), pageObj.getRecords());
	}
	@Transactional    //事物控制
	@Override
	public void saveItem(Item item,ItemDesc itemDesc) {
		
		//保存商品基本信息
		item.setStatus(1)
			.setCreated(new Date())
			.setUpdated(new Date());
		itemMapper.insert(item);
		//入库回显,mp会将入库后的数据回显到对象中,即将id值存到对象中
		
		//保存商品描述信息
		itemDesc.setItemId(item.getId())
				.setCreated(item.getCreated())
				.setUpdated(item.getUpdated());
		itemDescMapper.insert(itemDesc);
	}
	@Transactional
	@Override
	public void updateItemById(Item item,ItemDesc itemDesc) {
		item.setUpdated(new Date());
		itemMapper.updateById(item);
		//保存商品描述信息
		itemDesc.setItemId(item.getId())
				.setCreated(item.getCreated())
				.setUpdated(item.getUpdated());
		itemDescMapper.updateById(itemDesc);
	}
	@Transactional
	@Override
	public void deleteItemByIds(Long[] ids) {
		List<Long> idList = Arrays.asList(ids);
		itemMapper.deleteBatchIds(idList);
		itemDescMapper.deleteBatchIds(idList);
	}
//	@Transactional
//	@Override
//	public void reshelfItems(Long[] ids) {
//		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
//		queryWrapper.in("id",Arrays.asList(ids));
//		Item item =new Item();
//		item.setStatus(1);
//		itemMapper.update(item,queryWrapper);
//		
//	}
//	@Transactional
//	@Override
//	public void instockItems(Long[] ids) {
//		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
//		queryWrapper.in("id",Arrays.asList(ids));
//		Item item =new Item();
//		item.setStatus(2);
//		itemMapper.update(item,queryWrapper);
//		
//	}
	@Transactional
	@Override
	public void itemsRestFul(Long[] ids, Integer status) {
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("id",Arrays.asList(ids));
		Item item =new Item();
		item.setStatus(status);
		item.setUpdated(new Date());
		itemMapper.update(item,queryWrapper);
		
	}
	@Override
	public ItemDesc findItemDesc(Integer itemId) {
		ItemDesc itemDesc = itemDescMapper.selectById(itemId);
		return itemDesc;
	}
	
	
	
	
	
	
	
	
}
