package com.jt.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.pojo.ItemHot;
import com.jt.service.DubboItemService;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.params.SetParams;
@Service
public class DubboItemServiceImpl implements DubboItemService{

	@Autowired
	ItemMapper itemMapper;
	@Autowired
	ItemDescMapper itemDescMapper;
	@Autowired
	JedisCluster jedisCluster;
	@Override
	public Item findItemById(Long itemId) {
		
		return itemMapper.selectById(itemId);
	}
	@Override
	public ItemDesc findItemDescById(Long itemId) {
		
		return itemDescMapper.selectById(itemId);
	}
	@Override
	public List<ItemHot> findItemByHot() {
		//从redis中获取点击量最大的几个数据

		Set<String> result = jedisCluster.zrevrange("rank", 0, 5);
		if (result == null) {
			return null;
		}
		List<Long> ids =new ArrayList<>();
		for (String ss : result) {
			ids.add(Long.parseLong(ss));
		}
		
		
		List<ItemHot> list = new ArrayList<>();
		for (Long id : ids) {
			
			Item item = findItemById(id);
			ItemHot itemHot = new ItemHot("", "http://www.jt.com/items/"+id+".html", 1, item.getImage(), "");
			list.add(itemHot);
		}
		System.out.println("业务被调用:"+list);
		return list;
	}
}
