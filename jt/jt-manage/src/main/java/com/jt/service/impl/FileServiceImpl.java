package com.jt.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.jt.pojo.ImageVO;
import com.jt.service.FileService;
import com.jt.util.ImageTypeUtil;
@Service
@PropertySource("classpath:/properties/image.properties")
public class FileServiceImpl implements FileService {
	@Value("${image.localDir}")
	private String localDir;
	@Autowired
	ImageTypeUtil imageTypeUtil;
	@Value("${image.imageUrl}")
	String imageUrl;  //定义url虚拟网址
	
	@Override
	public ImageVO uploadFile(MultipartFile uploadFile) {
		//校验上传的信息是否为图片
				Set<String> typeSet= imageTypeUtil.getImageType();
				//获取用户上传图片类型
				String fileName = uploadFile.getOriginalFilename().toLowerCase(); //都装换成小写
				int index = fileName.lastIndexOf(".");
				String typeString = fileName.substring(index);  //文件后缀名
				if (!typeSet.contains(typeString)) { 
					return ImageVO.fail();
				}
				//自动生成目录结构,提高查询效率
				String dateDir= new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
				
				//创建文件目录(如果不存在)
				String dirPath= localDir+dateDir;
				File file = new File(dirPath);
				if (!file.exists()) {
					file.mkdirs();
				}
				//重新定义文件名及路径
				String uuid =UUID.randomUUID().toString();
				String realFileName = uuid+typeString;
				
				//执行上传文件
				File imageFile= new File(dirPath+realFileName);
				String url = imageUrl+dateDir+realFileName;
				try {
					uploadFile.transferTo(imageFile);
				} catch (Exception e) {
					
					e.printStackTrace();
					return ImageVO.fail();
				}
				
				return ImageVO.success(url);
		
	}

}
