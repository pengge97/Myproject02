package com.jt.service;

import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;

public interface ItemService {
	EasyUITable findItemByPage(Integer page,Integer rows);

	void saveItem(Item item, ItemDesc itemDesc);

	void updateItemById(Item item, ItemDesc itemDesc);

	void deleteItemByIds(Long[] ids);

//	void reshelfItems(Long[] ids);
//
//	void instockItems(Long[] ids);

	void itemsRestFul(Long[] ids, Integer status);

	ItemDesc findItemDesc(Integer itemId);


}
