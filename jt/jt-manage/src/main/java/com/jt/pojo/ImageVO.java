package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageVO {
	private Integer error;	//错误信息
	private String url;    //图片路径
	private Integer width; //宽度
	private Integer height; //高度
	//封装失败
	public static ImageVO fail() {
		return new ImageVO(1,null,null,null);
	}
	
	//封装成功
	public static ImageVO success(String url) {
		return new ImageVO(0,url,null,null);
	}
	public static ImageVO success(String url,Integer width,Integer height) {
		return new ImageVO(0,url,width,height);
	}
	
	
}
