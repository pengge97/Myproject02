package com.jt.util;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:/properties/image.properties")
public class ImageTypeUtil {

	/**
	 * 一般图片的类型都是常见的几种,变化范围不大
	 */
	@Value("${image.imageTypes}")
	private String imageTypes;
	private Set<String> typeSet = new HashSet<>();

	@PostConstruct // 当对象交给容器管理后,执行该方法
	public void init() {
		String[] types = imageTypes.split(",");
		for (String type : types) {
			typeSet.add(type);
		}
	}

	public Set<String> getImageType() {
		return typeSet;
	}
	

	/*
	 * static { typeSet.add(".jpg"); typeSet.add(".png"); typeSet.add(".gif");
	 * typeSet.add(".jepg"); typeSet.add(".bmp");
	 * 
	 * }
	 * 
	 * public static Set<String> getImageType() { return typeSet; }
	 */
}
