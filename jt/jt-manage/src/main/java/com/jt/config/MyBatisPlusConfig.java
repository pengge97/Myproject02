package com.jt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

/**
 * 标识配置类
 * @author asus
 *
 */
@Configuration
public class MyBatisPlusConfig {

	//一般和bean注解联用,表示将返回的对象实例化后交给spring管理
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
}
