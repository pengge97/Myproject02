package com.jt.test;

import org.junit.jupiter.api.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class TestHash {

	@Test
	public void testHash01() {
		
		//创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129",6379);
		jedis.flushAll();
		jedis.hset("user", "name", "tomcat");
		jedis.hset("user", "id", "100");
		System.out.println(jedis.hgetAll("user"));
	}
	@Test
	public void testList01() {
		
		//创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129",6379);
		jedis.flushAll();
		jedis.lpush("list", "1","2","3");
		System.out.println(jedis.rpop("list"));
	}
	@Test
	public void testTx01() {
		
		//创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129",6379);
		jedis.flushAll();
		Transaction transaction = jedis.multi();
		try {
			jedis.set("a", "a");
			jedis.set("b", "b");
			jedis.set("c", "c");
			jedis.set("d", "d");
		} catch (Exception e) {
			e.printStackTrace();
			transaction.discard();
		}
		
	}
}
