package com.jt.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;
public class RedisTest {
/**
 * 1.spring整合redis
 */
	@Test
	public void testString01() {
		//1.创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129", 6379);
		
		//2.操作jedis
		
		jedis.set("a", "redis入门案例");
		String value = jedis.get("a");
		System.out.println(value);
	}
	@Test
	public void testString02() {
		//1.创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129", 6379);
		jedis.set("a", "redis入门案例");
		//2.判断数据是否存在
		if (!jedis.exists("a")) {
			jedis.set("a", "bb");
		}
		
		
	}
	@Test
	public void testString03() {
		//1.创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129", 6379);
		jedis.flushAll();
		//setnx当且仅当当前key不存在时执行
		jedis.setnx("a", "aa");
		jedis.setnx("a", "bb");
		
		
		
	}
	@Test
	public void testString04() throws InterruptedException {
		//1.创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129", 6379);
		jedis.flushAll();
		//setnx当且仅当当前key不存在时执行
//		jedis.set("a", "aa"); //如果此时出现异常,则这个数据会永久存在
//		jedis.expire("a", 21);
		//原子性操作
		jedis.setex("a", 21, "aaaa");
		Thread.sleep(20000);
		System.out.println("剩余时间:"+jedis.ttl("a"));
		System.out.println("a:"+jedis.get("a"));
		jedis.close();
	}
	/**
	 * 
	 * SetParams 参数说明
	 * 	1.NX 只有key不存在时才能修改  
	 * 	2.XX 只有key存在时才能修改  
	 * 	3.PX 添加的时间单位是毫秒 
	 * 	4.EX 添加的时间单位是秒 
	 * 
	 */
	@Test
	public void testString05() throws InterruptedException {
		//1.创建jedis对象
		Jedis jedis = new Jedis("192.168.126.129", 6379);
		jedis.flushAll();
		SetParams params= new SetParams();
		params.xx().ex(20);
		jedis.set("aa", "测试", params);
		
		
	}
}
