package com.jt.test;

import java.util.Date;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.ItemDesc;

public class TestObjectMapper {	
	 private static final ObjectMapper MAPPER=   new ObjectMapper();
	/**
	 * 目的:实现对象与json串之间的转换
	 * 		1.将对象转换为json
	 * 		2.将json转换为对象
	 * @throws JsonProcessingException 
	 */
	@Test
	public void test01() throws JsonProcessingException {
		ItemDesc itemDesc = new ItemDesc();
		itemDesc.setItemId(101L).setItemDesc("json转换测试")
				.setCreated(new Date()).setUpdated(itemDesc.getCreated());
		
		//将对象转换成json
		String json = MAPPER.writeValueAsString(itemDesc);
		System.out.println(json);
		
		//将json转换成对象
		ItemDesc itemDesc2 = MAPPER.readValue(json, ItemDesc.class);
		System.out.println(itemDesc2);
	}
}
