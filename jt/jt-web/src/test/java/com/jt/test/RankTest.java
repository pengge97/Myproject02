package com.jt.test;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.SortingParams;
import redis.clients.jedis.Tuple;

@SpringBootTest
public class RankTest {
	@Autowired
	JedisCluster jedisCluster;
	
	
	@Test
	public void rankTest() {
//		jedisCluster.zadd("rank", 3, "666666");
//		jedisCluster.zadd("rank", 2, "1111111");
//		jedisCluster.zadd("rank", 0, "2222222");
		 Set<String> result = jedisCluster.zrevrange("rank", 0, 5);
//		 Set<Tuple> result = jedisCluster.zrangeByScoreWithScores("rank", 0, 100);
//		jedisCluster.zincrby("rank", 4, "2222222");
		for (String tuple : result) {
			System.err.println(tuple);
		}
		System.out.println(jedisCluster.zscore("rank", "11"));
	}
}
