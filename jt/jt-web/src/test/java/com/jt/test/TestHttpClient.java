package com.jt.test;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import com.jt.pojo.User;
import com.jt.util.ObjectMapperUtil;

public class TestHttpClient {

	/**
	 * 案例说明:	
	 * 	1.利用httpClient机制访问百度服务器 . htt://www.baidu.com
	 * 	2.实现步骤:(了解)
	 * 		1)定义请求网址
	 * 		2)定义httpClient工具API对象
	 * 		3)定义请求类型
	 * 		4)发起请求,获取返回值结果
	 * 		5)校验返回值
	 * 		6)获取返回值结果数据
	 * @throws IOException 
	 * @throws ClientProtocolException ]
	 * 
	 */
	@Test
	public void testGet() throws ClientProtocolException, IOException {
//		String url= "http://www.baidu.com";
		String url= "http://manage.jt.com/web/testCors";
		CloseableHttpClient httpClient =HttpClients.createDefault();
		HttpGet httpGet =new HttpGet(url);
		CloseableHttpResponse response = httpClient.execute(httpGet);  //不能保证网络请求一定正确
		
		//发起请求后  需要判断返回值结果是否正确,一般条件下判断响应状态码是否为200
		//404 400
		int status = response.getStatusLine().getStatusCode();
		if (status == 200) {
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity,"utf-8");
			User user = ObjectMapperUtil.toObject(result, User.class);
			System.out.println(user);
		}
	}
}
