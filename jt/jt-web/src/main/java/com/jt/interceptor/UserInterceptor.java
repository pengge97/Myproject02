package com.jt.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.jt.pojo.User;
import com.jt.util.ObjectMapperUtil;

import redis.clients.jedis.JedisCluster;

//mvc提供的对外拦截器
@Component
public class UserInterceptor implements HandlerInterceptor{
	@Autowired
	private JedisCluster jedisCluster;
	/**
	 * 目的:如果用户不登录,则不允许访问权限相关的业务
	 * 返回值:
	 * 		true:表示放行
	 * 		false:表示拦截     一般配置重定向使用
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//业务实现:1.获取cookie
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length >0) {
			for (Cookie cookie : cookies) {
				if ("JT_TICKET".equalsIgnoreCase(cookie.getName())) {
					
					String ticket = cookie.getValue();
					
					//redis中是否有该数据
					if (jedisCluster.exists(ticket)) {
						//从redis中获取用户信息
						String json = jedisCluster.get(ticket);
						User user = ObjectMapperUtil.toObject(json, User.class);
						//将用户信息添加到请求中
						request.setAttribute("JT_USER", user);
						return true;
					}
				}
			}
		}
		
		
		//重定向到用户登录页面
		response.sendRedirect("/user/login.html");
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}


	
	
}
