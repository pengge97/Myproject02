package com.jt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.Order;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.service.DubboOrderService;
import com.jt.vo.SysResult;

@Controller
@RequestMapping("/order/")
public class OrderController {
	@Reference(check = false)
	private DubboOrderService orderService;
	@Reference(check = false)
	private DubboCartService cartService;
	/**
	 * 跳转到订单确认页面
	 */
	@RequestMapping("create")
	public String create(HttpServletRequest request,Model model) {
		User user = (User) request.getAttribute("JT_USER");
		Long userId = user.getId();
		List<Cart> cartList = cartService.findCartListByUserId(userId);
		model.addAttribute("carts", cartList);
		return "order-cart";
	}
	
	/**
	 * form表单提交
	 */
	@RequestMapping("submit")
	@ResponseBody
	public SysResult saveOrder(Order order,HttpServletRequest request) {
		//获取用户名
		User user = (User) request.getAttribute("JT_USER");
		order.setUserId(user.getId());
		//执行入库操作
		Order orderDB =orderService.saveOrder(order);
		String orderId =orderDB.getOrderId();
		if (StringUtils.isEmpty(orderId)) {
			return SysResult.fail();
		}
		return SysResult.success(orderDB);
	}
	
	
	
	@RequestMapping("success")
	public String findOrderById(String out_trade_no,Model model) {
		Order order =orderService.findOrderById(out_trade_no);
		
		model.addAttribute("order", order);
		return "success";
	}
	

}





