package com.jt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.vo.SysResult;

@Controller
@RequestMapping("/cart/")
public class CartController {

	@Reference(check = false)
	private DubboCartService cartService;
	/**
	 * 展示购物车中的数据
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("show")
	public String show(Model model,HttpServletRequest request) {
		//获取UserId
		User user = (User) request.getAttribute("JT_USER");
		long userId = user.getId();
//		Long userId= 7L;
		//基于userId查询购物车数据
		List<Cart> cartList= cartService.findCartListByUserId(userId);
		model.addAttribute("cartList", cartList);
		return "cart";
	}
	
	/**
	 * 更新购物车操作
	 * 	1.url:http://www.jt.com/cart/update/num/562379/8
	 * 	2.请求参数: 562379--itemId    /8--num
	 * 	3.返回值结果:void
	 */
	@RequestMapping("update/num/{itemId}/{num}")
	@ResponseBody
	public void updateCart(Cart cart,HttpServletRequest request) {
		User user = (User) request.getAttribute("JT_USER");
		long userId = user.getId();
		cart.setUserId(userId);
		cartService.updateCartNum(cart);
	}
	
	/**
	 * 删除
	 *  URL:http://www.jt.com/cart/delete/1474391967.html
	 *  参数:1474391967  itemId
	 */
	@RequestMapping("delete/{itemId}")
	public String deleteByItemId(Cart cart,HttpServletRequest request) {
		User user = (User) request.getAttribute("JT_USER");
		long userId = user.getId();
		cart.setUserId(userId);
		cartService.deleteByItemId(cart);
		return "redirect:/cart/show.html";
	}
	/**
	 * 购物车新增
	 */
	@RequestMapping("add/{itemId}")
	public String saveCart(Cart cart,HttpServletRequest request) {
		User user = (User) request.getAttribute("JT_USER");
		long userId = user.getId();
		cart.setUserId(userId);
		cartService.saveCart(cart);
		return "redirect:/cart/show.html";
	}
}
