package com.jt.controller;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.vo.SysResult;

import redis.clients.jedis.JedisCluster;


@Controller
@RequestMapping("/user/")
public class UserController {
	@Reference(loadbalance = "leastactive",check = false)
	private DubboUserService dubboUserService;
	@Autowired
	private JedisCluster jedisCluster;
	/**
	 * 用户登录校验
	 * 实现单点登录 : 
	 * 		url:http://www.jt.com/user/doLogin
	 * 		参数:{username:username,password:password}
	 * 关于cookie参数说明:
	 * 		domain:指定域名可以实现cookie共享
	 * 		path:只有在特定路径下才能获取cookie  例:/aaa  只有在aaa路径下再能获取cookie
	 * @param user
	 * @return
	 */
	@RequestMapping("doLogin")
	@ResponseBody
	public SysResult doLogin(User user,HttpServletResponse response) {
		//将用户名密码提交业务层校验
		String ticket = dubboUserService.checkUser(user);
		if(StringUtils.isEmpty(ticket)) {
			return SysResult.fail();
		}
		//准备kookie实现数据储存
		Cookie cookie = new Cookie("JT_TICKET", ticket);
		cookie.setDomain("jt.com");
		cookie.setPath("/");
		cookie.setMaxAge(7*24*60*60); //设置为7天超时
		response.addCookie(cookie);
		
		return SysResult.success();
	}

	/**
	 * 用户登出操作
	 * 		删除redis中的用户信息
	 * 		删除cookie中的用户信息
	 */
	@RequestMapping("logout")
	public String logout(HttpServletResponse response,HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();//   new Cookie("JT_TICKET", "");
		if (cookies !=null && cookies.length !=0) {
			
			for (Cookie cookie : cookies) {
				if ("JT_TICKET".equalsIgnoreCase(cookie.getName())) {
					//删除redis中的用户信息
					jedisCluster.del("JT_TICKET");
					
					
					//删除cookie中的用户信息
					cookie.setDomain("jt.com");
					cookie.setPath("/");
					cookie.setMaxAge(0);
					response.addCookie(cookie);
				}
			}
		}
		return "login";
	}
	
	/**
	 * 完成用户的注册
	 */
	@RequestMapping("doRegister")
	@ResponseBody
	public SysResult doRegister(User user) {
		
		dubboUserService.saveUser(user);
		
		return SysResult.success();
	}
	/**
	 * 实现用户页面跳转
	 * http://www.jt.com/user/register.html
	 */
	@RequestMapping("register")
	public String register() {
		//跳转到登录页面
		return "register";
	}
	@RequestMapping("login")
	public String login() {
		//跳转到登录页面
		return "login";
	}
}
