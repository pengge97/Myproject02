package com.jt.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.pojo.ItemHot;
import com.jt.service.DubboItemService;
import com.jt.vo.SysResult;

@Controller
@RequestMapping("/items/")
public class ItemController {
	@Reference(check = false)
	private DubboItemService itemService;

	@RequestMapping("/{itemId}")
	public String findItemById(@PathVariable Long itemId, Model model) {
		Item item = itemService.findItemById(itemId);
		ItemDesc itemDesc = itemService.findItemDescById(itemId);
		model.addAttribute("item", item);
		model.addAttribute("itemDesc", itemDesc);

		return "item";
	}

	@RequestMapping("findItemByHot")
	@ResponseBody
	public SysResult findItemByHot() {
		List<ItemHot> list = itemService.findItemByHot();
	
		return SysResult.success(list);
	}
}
