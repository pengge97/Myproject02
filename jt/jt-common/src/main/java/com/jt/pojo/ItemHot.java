package com.jt.pojo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemHot implements Serializable{

	private static final long serialVersionUID = 7591542743986497837L;
	private String alt;// "",
	private String href; //"http://www.jt.com/items/562379.html",
	private Integer index; //1,
	private String src; //"http://img13.360buyimg.com/da/jfs/t283/161/1609640628/12590/ecd295d3/543f2a46N876265d2.jpg",
	private String ext; //""
}
