package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@TableName("tb_item_cat")
@Accessors(chain = true)
public class ItemCat extends BasePojo{

	private static final long serialVersionUID = 5006550716849464495L;
	private Long id ;//商品id，同时也是商品编号
	private Long parentId;
	private String name;
	private Integer status; //1.正常  2.删除
	private Integer sortOrder; //排序号
	private Boolean isParent; // 0 false  !0 真
	
}
