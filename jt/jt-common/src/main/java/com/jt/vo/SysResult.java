package com.jt.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysResult implements Serializable{

	private static final long serialVersionUID = 33381862901052641L;
	private Integer status;
	private String msg;
	private Object data;
	
	//准备工具ApI 方便用户使用
	/**
	 * 业务处理失败方法
	 * @return
	 */
	public static SysResult fail() {
		return new SysResult(201, "业务调用失败", null);
	}
	/**
	 * 业务调用成功
	 * @return
	 */
	public static SysResult success() {
		return new SysResult(200, "业务调用成功", null);
	}
	/**
	 * 业务调用成功
	 * @param msg 返回的消息
	 * @param data 返回的数据
	 * @return
	 */
	public static SysResult success(String msg,Object data) {
		return new SysResult(200, msg, data);
	}
	/**
	 * 业务调用成功
	 * @param data 需要返回的数据
	 * @return
	 */
	public static SysResult success(Object data) {
		return new SysResult(200, "业务调用成功", data);
	}
}
