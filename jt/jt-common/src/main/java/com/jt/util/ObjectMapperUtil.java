package com.jt.util;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtil {

	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	//对象 -->json
	public static String toJson(Object target) {
		if (target ==null) {
			throw new NullPointerException("target数据为null");
		}
		
		try {
			return MAPPER.writeValueAsString(target);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	
	//json --> 对象
	
	public static <T>T toObject(String json,Class<T> targetClass) {
		if (StringUtils.isEmpty(json) || targetClass ==null) {
			throw new NullPointerException("参数不能为空");
		}
		
		try {
			return MAPPER.readValue(json, targetClass);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	
	}
}
