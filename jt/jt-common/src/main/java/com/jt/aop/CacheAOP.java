package com.jt.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jt.anno.CacheFInd;
import com.jt.util.ObjectMapperUtil;

import redis.clients.jedis.JedisCluster;

@Component
@Aspect
public class CacheAOP {
	@Autowired(required = false)
//	private Jedis jedis;
	private JedisCluster jedis;
	/**
	 * 实现思路:拦截被@CacheFind标识的方法,之后利用AOP进行缓存控制
	 * 通知方法:环绕通知
	 * 实现步骤:
	 * 			1.准备查询redis的key  ITEM_CAT_LIST::第一个参数
	 * 			2.@annotation(CacheFInd) 动态获取注解的语法
	 * 			 拦截指定注解类型的注解并将注解对象当做参数进行传递
	 */
	
	
	@SuppressWarnings("unchecked")
	@Around("@annotation(cacheFInd)")
	public Object around(ProceedingJoinPoint joinPoint,CacheFInd cacheFInd) {
		
		//1.如何获取注解中的key
		String key = cacheFInd.key();
		//动态获取第一个参数当做key
		String firstArgs = joinPoint.getArgs()[0].toString();
		key += "::"+firstArgs;
		
		Object result = null;
		if (jedis.exists(key)) {
			//key存在 从缓存中获取数据
			String json = jedis.get(key);
			MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
			result =ObjectMapperUtil.toObject(json, methodSignature.getReturnType());
			System.out.println("aop查询redis缓存");
		}else {
			//key不存在,需要从数据库查询
			
			try {
				result = joinPoint.proceed();
				String json = ObjectMapperUtil.toJson(result);
				int seconds = cacheFInd.seconds();
				if (seconds>0) {
					jedis.setex(key, seconds,json);
				}else {
					jedis.set(key, json);
				}
			} catch (Throwable e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
			System.out.println("数据库查询");
		}
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//公式:    切面= 切入点表达式+通知方法
	/**
	 * 拦截itemCatServiceImpl类中的业务
	 * @Pointcut 
	 */

	//@Pointcut("bean(itemCatServiceImpl)") //按类匹配,控制力度较粗 ,单个bean
	//@Pointcut("within(com.jt.service..*)")  //按类匹配,控制力度较粗 ,多个bean
	//@Pointcut("execution(* com.jt.service..*.(...)") //控制力较细
	//public void pointCut() {}
	
	/**
	 * JoinPoint 方法执行时切入点恰好被切入点表达式匹配,该方法的执行就称之为连接点
	 * @param jp
	 */
	/*
	@Before("pointCut()")
	public void before(JoinPoint jp) {
		//执行方法的全类名
		String typeName = jp.getSignature().getDeclaringTypeName();
		//执行方法的方法名
		String methodName = jp.getSignature().getName();
		//获取方法参数
		Object[] args = jp.getArgs();
		//获取目标对象
		Object target = jp.getTarget();
		
		System.out.println("我是前置通知");
	}
	//添加环绕通知
	@Around("pointCut()")
	public Object around(ProceedingJoinPoint joinPoint) {
		System.out.println("环绕通知开始");
		try {
			Object result = joinPoint.proceed();
			System.out.println("环绕通知结束");
			return result;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	*/
	
	
	
	
	
}
