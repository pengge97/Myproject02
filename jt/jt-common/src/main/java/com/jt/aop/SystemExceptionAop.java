package com.jt.aop;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.vo.SysResult;

import lombok.extern.slf4j.Slf4j;

/**
 * 全局异常处理
 * @author asus
 *
 *常规的使用SysResult  但是对于跨域访问不行
 *如何分开处理?跨域访问有callback
 */
//标识是全局异常处理机制的配置类
@RestControllerAdvice
@Slf4j
public class SystemExceptionAop {

	/*
	 * 通用异常返回的方法
	 */
	@ExceptionHandler(RuntimeException.class)
	public Object systemResultException(HttpServletRequest request,Exception e) {
//		e.printStackTrace();  //在控制台打印异常
		
		String callback = request.getParameter("callback");
		if (StringUtils.isEmpty(callback)) {
			log.error("{~~~~~~~~~~~业务调用异常}",e);
			return SysResult.fail();
		}
		//有可能            跨域jsonp只能get请求
		String method= request.getMethod();
		if (!method.equalsIgnoreCase("get")) {
			log.error("{~~~~~~~~~~~业务调用异常}",e);
			return SysResult.fail();
		}
		//当上面的情况都不满足后就表示是跨域访问
		log.error("{~~~~~~"+e.getMessage()+"}", e);
		return  new JSONPObject(callback, SysResult.fail());
	}
}
