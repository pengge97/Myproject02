package com.jt.aop;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jt.anno.InsertOperation;
import com.jt.pojo.Order;
import com.jt.pojo.OrderItem;

import redis.clients.jedis.JedisCluster;
@Component
@Aspect
public class RankAOP {
	@Autowired
	JedisCluster jedisCluster;
	
	@Around("@annotation(insertOperation)")
	public Object around(ProceedingJoinPoint joinPoint,InsertOperation insertOperation) {
		try {
			insertOperation.key();
			//获取返回值中的订单信息
			Order order = (Order) joinPoint.proceed();
//			System.err.println("执行了热度排行方法");
			//查询订单对应的商品信息
			Date nowdDate = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(nowdDate);
			List<OrderItem> orderItems = order.getOrderItems();
			for (OrderItem orderItem : orderItems) {
				//获取商品id
				String id = orderItem.getItemId();
				//在redis中查询对应的商品销量
				Double result = jedisCluster.zscore("rank", id);
				if (result == null) {
					//如果没有,就新增
					jedisCluster.zadd("rank:"+date, orderItem.getNum(), id);
				}else {
					//如果有就在原有基础上加
					jedisCluster.zincrby("rank:"+date, orderItem.getNum(), id);
				}
			}
			System.out.println(order);
			return order;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
}
