package com.jt.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
@Component
@Aspect
public class ServiceAOP {
	
	@Around("within(com.jt.service.impl.*)")
	public Object around(ProceedingJoinPoint joinPoint) {
		Object result=null;
		try {
			result = joinPoint.proceed();
			String name = joinPoint.getSignature().getDeclaringTypeName();
			String method = joinPoint.getSignature().getName();
			Object[] args = joinPoint.getArgs();
			String out = name+"."+method;
			for (Object obj : args) {
				System.out.println(obj);
			}
			System.out.println(out);
			return result;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
//	@AfterThrowing("within(com.jt.service.impl.*)")
//	public void afterThrowing(JoinPoint joinPoint) {
//		String name = joinPoint.getSignature().getDeclaringTypeName();
//		String method = joinPoint.getSignature().getName();
//		Object[] args = joinPoint.getArgs();
//		String out = name+"."+method;
//		for (Object obj : args) {
//			System.out.println(obj);
//		}
//		System.out.println(out);
//	}
}
