package com.jt.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD) //注解对谁生效
@Retention(RetentionPolicy.RUNTIME) //直接使用有效期
public @interface CacheFInd {
	public String key();  //标识存入redis的key值
	public int seconds() default 0;  //保存的时间单位
}
