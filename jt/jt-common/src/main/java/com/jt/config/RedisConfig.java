package com.jt.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

/**
 * 
 * @author asus
 *
 */

@Configuration  
@PropertySource("classpath:/properties/redis.properties")
public class RedisConfig {
	/*
	 //单台测试
	@Value("${redis.host}")
	private String host;
	@Value("${redis.port}")
	private Integer port;
	
	@Bean
	public Jedis jedis() {
		return new Jedis(host,port);
	}
	*/
	
	/**
	 * 整合分片实现redis内存扩容
	 */
	@Value("${redis.nodes}")
	private String redisNodes ;
	@Bean
	public  JedisCluster jedisCluster() {
		String[] nodes= redisNodes.split(",");
		//动态获取redis节点信息
		Set<HostAndPort> set = new HashSet<>();
		for (String node : nodes) {
			String[] data = node.split(":");
			String host = data[0];
			Integer port = Integer.parseInt(data[1]);
			set.add(new HostAndPort(host, port));
		}
		return new JedisCluster(set);
	}
}
