package com.jt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//类似于web项目中的web.xml文件
@Configuration
public class CorsConfig implements WebMvcConfigurer {
	/**
	 * 配置后端的服务器可以跨域清单
	 * 参数说明: addMapping: 什么样的请求可以跨域  /web/**    /aaa/b/c/e/d/d/d
	 * 
	 * 			/*   匹配一级目录
	 *          /**  匹配多级目录   使用最多
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*") //http://manage.jt.com
				.allowedMethods("GET","POST","PUT","DELETE","HEAD") //允许请求的方式
				.allowCredentials(true)  //是否允许携带cookie
				.maxAge(1800);  	//允许跨域的时间
	
	
	}
}
