package com.jt.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号,开发时使用沙箱提供的APPID，生产环境改成自己的APPID
    public static String APP_ID = "2021000116681714";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCknbSqL/9cAQkaAYTE6gyKaIw0mwrIVr4aq1XOhQKeVETyW5BOIEG3L6HmgVopfiyNHo8tpQ4QpSxAm//reOcB0rmY+lSkquGsJJqGukBgPkqgM76EqpVfJINc1o9+X8ROiEdP8+z/lFELW0C9LM1a5pvBHwFgWuSGJij5w+CDESGLBIjKzCYb7wh79C3B02jCK+MpGTly18aiZxUhyJVDPnDLRjZGw6OVK8Yi34ZhZwDQ2CcRjHTmtZUqT4RYRkzMleaokI6BHzpxcbBDiqqSsqlFqHtQ6MYinHu9M+pcRlhVaOQFmgMuQHTq3NeLeDXnSG/NcLdaNdLkMRILZv7AgMBAAECggEAZoTLjNWTS1MRK6mPwc9yqPOc3MKScetCRm/W44Xbd2VSne1dZYoDGf2HemViAjCpKCRUX9cHo9kZBWc2Ye5aZiTFhz0PfA2ByRcSHkwNh1hQcN+rrsHU8/QN9H5aBxuFoysQtZugscMHSLfyn0MVhckrduFTA4eciT7idkHRvwARMckApHCRPuIgWTSBvMiq+dnbr4PFojhWf9Iq4JnPpF1D2Fikcz0C4oeruBce4LXqDbcAH1w/64q8p8zNX8uXYy7c5/6fAJHJ7JzrXh3XH+C3LeycT6Hu9F4Rx/3eT2nth6EC5MIp3o6HHMExBTzSf2EGadTP3xWBT7hbWPouYQKBgQD4hMVum3U8pTm02w7Xlx7+wias9FKndcRDunI9cYhE+jT9rbPhflmBU7MskQU0jUhGY8lK7P81vsvsOQoDxMxmHpcheREVzFK2T42v0EbanwcNyLNTj7B1JNfvsceiyysGbRz6nRMnI/NGOfyYWF+68S9ivwNek8Kb/NF2o+DyCwKBgQDIbfMnTdiITQJzOWjIqNs9Wy/MH9J75M60xQIQ7TxwwjweeJ6oFGHqOp6Osmng5fX64+tKwZQqeEtWAHowHaU7Bp9RZF/yc9eFtyInH806GgxugGLnbtMaVU/3N9lCF2TbGHhPOzLjjTBbqncZbWtXVxFHBu8lGiPYYsntMCGj0QKBgQDGGI+H60M4ZDvrffIGx99dE0Xg96Aq/CTGGAiYEbedxBQZYizQwfZ2sFFSZQbRLQ0/jqsVnnSQUtYaJh1lg6mqfRfbBrYnugb0a9V1H4BbJAhrZGANa6wstIGr/vozOd/B75f57Nsnh/CTOgWvKkKksTwkaj41VXL7BDuzZk4YoQKBgFVWezFA8/EU2PY2e2b0fmShbrh61CPsSBavyzFp0VIxYWeX8/WcuBQC4X/gdzRKtXEdmhE6Wtu6XFlB2sSh2rhHGY6OLkwNBrboQqJl5/vVG3XxNCtWBIwxxtp77QU28LpYsOELU5+PfgeJjSxGam3FpJWZXjlt59U6Or/CvpABAoGAZxnrWtmAqPYPTfg+og6bObXDBE1pYmwfXY+n4J6SFwpi5cKIBX0L3+wAUa/+SjDEORnxqEDPg+JeU5N7++nbXhiCT4cLqpTnvkSqu/6EhooaYdsmK4cTLe5uJV7BqWa0nXQSiuPPL5IieMbzXGIUh1eLlRBznTv5gbrlZvYofRQ=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String ALIPAY_PUBLIC_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCy7UTBQ/DtPwAW1g7OrG+fUvSh8vtKacKKUMo18lGpWU/OTxWNuNUcNeyPv0Tn0RWwVoqtEW6OaCtfjo65hu8u1qlO7fJgk5hW0av5QCk5J3gl5wb8Vhj9qI2lEteN2YzNw77Pftk8bt0YpMdoPx1Y9az6H3p3fzGyf3bJtJ/OCrQHVnO37jPpL7aTN4NPCV0eANp3vzBu6K8N4GUdhPgtXi0YFAnmhBymUY+A3hvcPaDpjs873R6KeLxem4F55I/96Wx6CnAl6oTZrmDzGJE8BT7kjl/QvsFqfGPeYxPfYui5HR+loiz/uy2/uLcUJEcHMt/mzJKIyVqbXx7rg8jvAgMBAAECggEAFryf6A9CJmPDFyPMbebrqEtaWtoNpL8bwYfSqlhImESJJ6ZVDtVV/2e6z+l2UB75qpbUuHlmmyIRoOr0qA7FhiYxDZ6fFjarPR0XxWyp0GF7mxVohdhLBLDOjxN94MCsa9lb/fIYmN1dqywFSvH6QLu3Z3FolpbLMeuQpMEXLY9oEe0jHAE2rtooaPB1lR4uzEfNRh0E7Sgb6wisnel+mB5RxiHr+T+lGZWOBAr/djfRrDhlMeQ6Z/35Fzi658U0qYZUJtuvCycg64vFnUyPIaSyMCoa2EvV3ht1HKyIcJNLYuXpfMkfwTHa7YLOiXsreEkJUoTPBka0TSCxhjwQAQKBgQDnZXyUhATd/QCJ88YG3IdLJTDgEcbS0U2tsrbJtEdYBKkFMpJZoxHlToTEvLpXSQZyt1k9pKs6isH0GQgKHd20MDcqiUOHXb5gpUXhPV6WOnl7Z6JkTtcEDyXh6iZagx7ViTiG3+7aOES17p9p/gQ8HVUhB/GYu9KDejm4udwHAQKBgQDF85TcN3ejoWZIUPgMYilraP5ooiZCQH5yIrTOmvleNB1d3Io7G/z23/TjjgG5OblGK/9sYwRDh/DJGIRmYDQ3kdixLoHe+yIt9Y4Wq5O6I5xeRDqVYL84QXnSHnwDeG5/M0u/TGIH8vDO5Bqm9duGgPeEepQaqKNlJuUCoWA/7wKBgBklbD5vgl/laLOVLEX+nS1LY15WUDcEy94+d6K/1qj4CK4p6sGW+RDkUYVXxSBLLUo2JpqOVJVOEvnF5pvC1x8Hxlb3B8GkLFYwX8G0e5zzydPhVUJ1VuR4CPPeEEDx0t39xYjSuRA54S2sOuK32hn2ThQC2vxwHNZw5+jtOb4BAoGBAIG9lpGHYUjEHk/DkfA73j3iAOz/aef+cWHJUYwgktSlf6HnTEpVm8ULIGyMxKBsLvzJFY4qj77bOHv1L8IRx+esFcR7YO+O1eYdjVVae2aCtJgsS+FgGbR7ZabMCCgWJQYFzt3A6HehSSew5a8McSeFG7Weybd3VDCV/39r2bPNAoGBANBxc+dCdmkeHURi7Kb8UEOcCBj9ZBudGgp10r5gioI6rLJBHUX4YHHeFZQO98DMdqBB4coMIUzXq4UW9pmJ7/MkwJFt9gTgkNXCp5p8u465et+dZNIbBiPri22aOs1iTn5U+ZxjksaFfdnaOjfdWZNG23Et+74e8dH0W9/t6uZ7";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://www.jt.com/order/success.html";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "utf-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "D:\\log\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
