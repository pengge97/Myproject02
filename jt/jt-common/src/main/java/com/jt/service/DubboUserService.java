package com.jt.service;

import com.jt.pojo.User;
import com.jt.vo.SysResult;

public interface DubboUserService {

	void saveUser(User user);

	String checkUser(User user);

}
