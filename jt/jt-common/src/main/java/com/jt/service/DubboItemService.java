package com.jt.service;

import java.util.List;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.pojo.ItemHot;

public interface DubboItemService {

	Item findItemById(Long itemId);

	ItemDesc findItemDescById(Long itemId);

	List<ItemHot> findItemByHot();

}
