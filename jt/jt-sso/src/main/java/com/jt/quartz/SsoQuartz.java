package com.jt.quartz;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.Order;
import com.jt.pojo.User;

import redis.clients.jedis.JedisCluster;

//准备订单定时任务
@Component
public class SsoQuartz extends QuartzJobBean{

	@Autowired
	UserMapper userMapper;
	@Autowired
	JedisCluster cluster;

	/**当用户订单提交30分钟后,如果还没有支付.则交易关闭
	 * 现在时间 - 订单创建时间 > 30分钟  则超时
	 * new date - 30 分钟 > 订单创建时间
	 */
	@Override
	@Transactional
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		//实现数据更新
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("state", 0);
		List<User> list = userMapper.selectList(queryWrapper);
		for(User user: list) {
			if(cluster.get("userlocked"+user.getUsername() )== null) {
				User u = new User();
				u.setState(1);
				QueryWrapper<User> wrapper = new QueryWrapper<>();
				wrapper.eq("username", user.getUsername());
				userMapper.update(u, wrapper);
			}
		}
		
		System.out.println("执行定时任务");
	}
}
