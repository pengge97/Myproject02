package com.jt.aop;


import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.anno.UserFind;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;


import redis.clients.jedis.JedisCluster;


@Aspect
@Component
public class UserAop {
	@Autowired
	JedisCluster jedisCluster;
	
	@Autowired
	UserMapper userMapper;
	
	
	
	@Around("@annotation(userFind)")
	public Object aroud(ProceedingJoinPoint joinPoint,UserFind userFind) {
		
		 User user = (User) joinPoint.getArgs()[0];	
//		 String userState="state"+user.getUsername();
		 try {
			String ticket = (String) joinPoint.proceed();
			if(ticket==null) {
				doTicketNull(user);
				return null;
			}else if("2".equals(ticket)){
				return null;
			}else {
				
				return ticket;
			}
			
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
		 
		 
		return null;
	}
	
	public void doTicketNull(User user) {
		String userState = "state" + user.getUsername();
		//判断redis里面是否有当前用户错误密码记录
		if (jedisCluster.exists(userState)) {
			//如果有,获取记录的错误次数
			Integer value = Integer.parseInt(jedisCluster.get(userState));
			//当次数大于等于3次,锁定账户
			if (value >= 3) {
				User userd = new User();
				userd.setState(0);
				QueryWrapper<User> updateWrapper = new QueryWrapper<>();
				updateWrapper.eq("username", user.getUsername());
				userMapper.update(userd, updateWrapper);
				jedisCluster.setex("userlocked" + user.getUsername(), 30 * 60, new Date().toString());
			} else {
				//次数少于3就在原有基础上加一次
				value++;
				jedisCluster.set(userState, value.toString());
			}
		} else {
			//如果没有记录就添加一条记录,值为 1
			jedisCluster.setex(userState, 60, "1");
		}
	}
	
}