package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.service.UserService;
import com.jt.vo.SysResult;

import redis.clients.jedis.JedisCluster;

@RestController
@RequestMapping("/user/")
public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	JedisCluster jedisCluster;
	@RequestMapping("/getMsg")
	public String getMsg() {
		return "单点系统登录搭建完成";
	}
	/**
	 * 
	 * @param param 需要检验的参数
	 * @param type	检验的参数类型
	 * @param callback 返回函数
	 * @return
	 */
	@RequestMapping("check/{param}/{type}")
	public JSONPObject checkUser(@PathVariable String param,@PathVariable Integer type,String callback) {
		
		boolean flag= userService.checkUser(param,type);
		SysResult sysResult = SysResult.success("ok", flag);
//		int a = 1/0;
		return new JSONPObject(callback, sysResult);
	}
	


	@RequestMapping("query/{ticket}")
	public JSONPObject findUserByTicket(String callback,@PathVariable String ticket) {
		if (jedisCluster.exists(ticket)) {
			String data = jedisCluster.get(ticket);
			return new JSONPObject(callback, SysResult.success(data));
		}else {
			return new JSONPObject(callback, SysResult.fail());
		}
	}
}
