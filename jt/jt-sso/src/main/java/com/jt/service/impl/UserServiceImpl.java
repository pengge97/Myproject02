package com.jt.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.UserService;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;
	private static Map<Integer, String> paramMap; 
	static {
		Map<Integer, String> map =new HashMap<>();
		map.put(1, "username");
		map.put(2, "phone");
		map.put(3, "email");
		paramMap=map;
	}
	/**
	 * 数据库中有数据  返回true    没有数据返回false
	 * 
	 * type: 1.username  2.phone  3.email
	 */
	@Override
	public boolean checkUser(String param, Integer type) {
		//将type转化成对应字段
		
		
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(paramMap.get(type), param);
		Integer count = userMapper.selectCount(queryWrapper);
		return count>0?true:false;
	}

}
