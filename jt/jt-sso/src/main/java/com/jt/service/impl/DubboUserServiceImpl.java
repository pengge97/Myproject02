package com.jt.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.anno.UserFind;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.util.ObjectMapperUtil;
import redis.clients.jedis.JedisCluster;

@Service  //dubbo service注解
public class DubboUserServiceImpl implements DubboUserService{

	@Autowired
	UserMapper userMapper;

	@Autowired
	private JedisCluster jedisCluster;
	//将数据进行加密处理
	@Transactional
	@Override
	public void saveUser(User user) {
		String password = user.getPassword();
		password = DigestUtils.md5DigestAsHex(password.getBytes());
		user.setPassword(password)
			.setEmail(user.getPhone())
			.setState(1)
			.setCreated(new Date())
			.setUpdated(new Date());
		
		userMapper.insert(user);
		
	}
	@UserFind(key = "USER_STATE")
	@Override
	public String checkUser(User user) {
		//对密码进行加密
		String password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
		user.setPassword(password);
		QueryWrapper<User> queryWrapper =new QueryWrapper<>(user);
		User obj = userMapper.selectOne(queryWrapper);
		System.err.println(obj);
		//数据库中是否存在该用户
		if (obj == null ) {
			return null;
//			throw new RuntimeException("请检查用户名或密码");
		}
		if(obj.getState() ==0) {
			return "2";
		}
		//判断密码是否正确
		UUID uuid = UUID.randomUUID();
		String ticket=uuid.toString().replace("-", "");
		//去除敏感数据
		obj.setPassword("你猜猜");
		jedisCluster.setex(ticket,7*24*3600,ObjectMapperUtil.toJson(obj) );
		return ticket;
		
	}
	
}
