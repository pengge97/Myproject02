package com.jt.conf;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jt.quartz.SsoQuartz;

@Configuration
public class SsoQuartzConfig {
	
	//定义任务详情
	@Bean
	public JobDetail ssojobDetail() {
		//指定job的名称和持久化保存任务
		return JobBuilder
				.newJob(SsoQuartz.class)
				.withIdentity("ssoQuartz")
				.storeDurably()
				.build();
	}
	//springboot会自动装配
	//定义触发器
	@Bean
	public Trigger ssoTrigger() {
		/*SimpleScheduleBuilder builder = SimpleScheduleBuilder.simpleSchedule()
				.withIntervalInMinutes(1)	//定义时间周期
				.repeatForever();*/
		//设定程序多久执行一次
		CronScheduleBuilder scheduleBuilder 
			= CronScheduleBuilder.cronSchedule("0 0/1 * * * ?");
		return TriggerBuilder
				.newTrigger()
				.forJob(ssojobDetail())  //执行什么样的任务
				.withIdentity("ssoQuartz")  //任务名称
				.withSchedule(scheduleBuilder).build();   //什么时候执行
	}
}
